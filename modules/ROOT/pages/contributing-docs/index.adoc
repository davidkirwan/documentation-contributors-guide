= Contribute to Improve and Expand Docs Articles
Fedora Documentation Team <https://discussion.fedoraproject.org/tag/docs>
:revnumber: F36 and newer
:revdate: 2023-05-01

:page-aliases: contributing

[abstract]
____
This document explains how to work with the publishing system used to build the Fedora Documentation website. It will guide you through contributing to existing documentation as well as creating completely new content sets and publishing them in the English originals as well as any possible translations.
____

There are many different ways to contribute to Fedora documentation. Some of them are designed to enable contributions without technical knowledge about Web Content Management Systems and about how to store and manage your contribution. The tool takes over all these issues for you. It enables authors to concentrate on the topic at hand.

Please check us out. If you come across a documentation page that contains an error or inaccuracy, use one of the casual contribution tools described below to improve the page. If you have any questions, please do not hesitate to contact the Docs team.

Local authoring tools are designed to provide a powerful working environment for accomplished authors. They enable efficient work even on large complex interrelated text collections. These tools are aimed at experienced authors. 

== How it works

Fedora documentation uses the Antora to build and manage the Web site. Documentation is once created static content, with updates from time to time. This is exactly what Antora specializes in. It gathers static text documents and transforms them into a complete Web site, including navigation, links, formatting, positioning, adaptation to different output devices, etc. For more general information about the Antora publishing system, see the https://antora.org/[Antora website] and https://docs.antora.org/antora/latest/page/[Antora documentation]. 

As a writer, you concentrate on the content and write away.

=== The general procedure

The 4-eyes principle applies to the Fedora documentation. A different author reviews each contribution. When you complete your text or text modification, the system creates a "Pull Request" or "Merge Request" to integrate your text into the documentation body. This triggers other authors, board members or members committed to the part of the documentation body in question, to start a review and either initiates an inclusion or starts a discussion. Allow 2 to 3 days for an answer to a request.

=== Some technical background

Fedora uses _AsciiDoc_ to format text in a simple and efficient way. It closely follows natural writing styles in everyday notes for structuring and highlighting. In this way, you can use any editor, including almost any word processor that can edit and save AsciiDoc Text.  More about this below.

The AsciiDoc text document is stored in series of Git repositories. This is a system especially popular among software developers, but also very capable for managing text documents. Git encourages and facilitates the use of the 4-eyes principle by a "Pull (or Merge) Request Workflow". You only need to worry about the details of this workflow if you want to set up a local work environment intended for professional and frequent contributions. All other tools take care of the necessary steps in the background. 


== Prerequisites

The only requirements for contributing documentation to Fedora Docs are:

* link:++https://admin.fedoraproject.org/accounts/++[*Fedora Account System*] (*FAS*) account.
* GitLab account
* Must have signed https://fedoraproject.org/wiki/Legal:Fedora_Project_Contributor_Agreement[Fedora Project Contributor Agreement] from FAS (see https://admin.fedoraproject.org/accounts/group/view/cla_done[here])
* Basic knowledge of *AsciiDoc* markup language (see xref:contributing-docs/asciidoc-markup.adoc[AsciiDoc for Fedora])


== The tools

=== Quick way: 'Edit' button

- Make changes to a single file for minor fixes directly from web UI of Git forge.
- Write access to upstream repo required.
- Use it as an exception (without review process), rather than a recommended option.
   
=== Easy way: Web IDE of Git forge

- Make changes to multiple files directly from web UI of Git forge.
- No need to install anything in local computer or run Git commands in terminal.
- Pull Requests for review process (Merge Requests in GitLab)
- You do not need special permissions or write access to the original project.
   
=== Flexible and advanced way: Create a local writing environment

- Work with multiple files and repos offline at your pace using your choice of text editor and terminal.
- You can build and render the pages locally to test your changes using Podman container.

== Typical ways to contribute

One can distinguish several typical types of contribution to Fedora documentation, for which the available tools are suitable in different ways.

There are many ways for various types of contribution. For example, typo fixes, adding short information or a link, update an article, write a new article.

=== Update an existing documentation page 

This task involves minor changes. For example, typo fixes, adding short information or a link, or a correction to the text. This type is especially necessary whenever the documentation needs to be updated for a new software version.

The _File Edit button_ are convenient for this purpose. 

=== Extend an existing documentation domain
   
This task involves adding one or more new chapter(s) or section(s) with several chapters. For example, adding another container technique to the documentation of containerization. 

The _Web IDE_ is a convenient tool for this purpose. A local writing environment is useable as well, but may involve too much overhead if you want contribute to just one document.

It is highly recommended to present the plan on one of the Docs communication channels before starting. You may get suggestions and tips on content, but also on editing. For example, file structure and naming conventions.

=== Introduce a new documentation area

This task involves a new extensive subject area, such as new software or a new administration tool. For example, inclusion of several sections and chapters and the creation of a new, separate repository.

For this type, setting up a local writing environment is useful and worthwhile.

In any case, a prior discussion with the Docs team is necessary, if only to create the technical prerequisites.
