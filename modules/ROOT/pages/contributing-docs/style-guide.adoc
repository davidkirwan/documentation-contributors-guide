= Fedora Documentation Style Guide
Fedora Documentation Team <https://discussion.fedoraproject.org/tag/docs>
2023-04-21
:page-pagination:

[abstract]
This guide provides style guidelines and preferred term usage for the Fedora Linux Documentation. Docs team created this guide to strive for correctness and consistency in documentation.

== Writing style guide

Write clearly and use shorter sentences:: The longer a user documentation is, the more difficult it is to understand. Writing with brevity (to the point) is good. Clarity is better.

Avoid passive voice:: Passive voice is the use of the object of a sentence as the subject. For example
* Active voice: The troops defeated the enemy.
* Passive voice: The enemy was defeated by the troops.

Be careful of gerunds (-ing):: They indicate passive voice. Rewrite your sentence to sound strong. For example:
* Weak, passive voice: Setting the foobar configuration option will make the application listen on all interfaces.
* Strong, active voice: Set the foobar configuration option to make the application listen on all interfaces.

Avoid unnecessary future tense:: Unless you are actually talking about future plans or publications, present tense is best.
* Unnecessary future tense: When you select Run, your program will start.
* Present tense: When you select Run, your program starts.

Avoid too much use of the verb to be in sentences:: Too much use of *is* makes your sentences weak. Try using the verb form of words you’ve shuffled off elsewhere in the sentence. Often you can simply drop words, or use the imperative (commanding or advising) form of the sentence.
* Weak: Zambone is an app used for managing your private documents on a server.
* Strong: Zambone manages your private documents on a server. Or: Use Zambone to manage your private documents on a server.
* Weak: When setting up a file server, it is important to plan the directory structure carefully.
* Strong: Plan the directory structure of the file server carefully before you set it up.

Use standard US English for spelling and other international differences:: US English is the lingua franca for the Fedora Project overall.

Have a smooth flow from general information to specific instructions:: Structure your article with abstract, bullet points and essential AsciiDoc markup.

Avoid long texts:: Verbose writing is daunting to read. Instead, organize the text using paragraphs, bullets, and numbered steps. This helps the user to quickly grasp the text and, above all, does not seem daunting. Provide a short abstract at the beginning of a longer collection of paragraphs:: Especially after second and third order headings ("h2" and "h3", "==" and "===" in AsciiDoc), a short sentence or paragraph should follow, briefly describing the goal and/or subject of the subsequent paragraphs, bringing the message to the reader's attention and aligning the reader's expectation.

== Typographic style guide

Use capital case for title (h1) and just title:: Capitalize the article title only.
+
* Incorrect: Fedora documentation style guide
* Correct: Fedora Documentation Style Guide

Use sentence case for the post title and heading titles:: Do not capitalize words in your article title or any heading, other than proper nouns.
+
* Incorrect: Technical Notes and Processes
* Correct: Technical notes and processes

Less is more:: Use boldface only for an extremely important phrase or statement.

Use Italics for system objects mentioned in a sentence:: GUI or CLI elements like button text, menu entries, or prompts the reader find on the screen commands or package names

Use the preformatted source text for command line input or output:: Use a shell prompt ($ or #) to indicate privilege level and set the input apart from output.

  [source,console]
  ----
  $ command arg1 arg2
  output line1
  output line2
  ----

Use of admonitions:: Tips, hints, and warnings, when used in abundance, interrupt the flow of writing and reading. Use admonitions when absolutely necessary.

== Content tips

These tips are about things to do — and avoid — in what you tell users to do. Remember that thousands of readers trust Fedora Documentation to tell them how to carry out tasks. Be responsible and helpful, test your examples carefully, advocate best practices, and respect the user’s security and choice.

Test your process:: If possible, use a fresh guest virtual machine — or at least a brand-new user account. Run your process from beginning to end to ensure it works. Fix, rinse, and repeat!

Use free and open source software and officially packaged software:: The article could cover non-FOSS software to be used on Fedora, where there is no alternative FOSS software for Fedora users.

Use Fedora family distributions:: Unless your documentation article specifically targets a cross-distribution mechanism, use installations, containers, or distributions within our family (Fedora, CentOS, RHEL).

Copr software must be accompanied by a caveat:: The Copr build system is not managed by the Fedora release team and does not provide official software builds. If you cover some software from Copr, ensure there’s no official build. If not, include a statement like this: Copr is not officially supported by Fedora infrastructure. Use packages at your own risk.

Avoid exclusionary language:: These are examples of terminology to avoid in articles:
+
* blacklist/whitelist — Use allowlist/denylist instead, which is more directly descriptive of the purpose.
* master/slave — Use primary/secondary, primary/replica, active/passive, active/standby, or another similar construct.

Use the correct style for third parties:: Names of companies, projects, and technologies do not always follow the style rules of typical English words. Choose the styling used by authoritative websites when you are unsure. If authoritative sources use inconsistent style, use your best judgment. A non-exhaustive list:

* Copr instead of COPR
* NVIDIA instead of Nvidia or nVidia
* Perl instead of PERL
* Red Hat instead of Redhat or RedHat
* ThinkPad instead of Thinkpad


== Images and screenshots

Use a fresh, standard Fedora system:: Do not use your personal system or setup. It is best to make a virtual machine with a fresh Fedora variant install, and do the steps there.

Set screen resolution at a reasonable but not too high:: Desktop environment specific screen capture software produces right-sized images for the articles. If you are showing a browser window, use active window option in screen capture software. Use an option not to include window title bar.

If you are only showing an application, pop-up, or specific areas, use an option in the software to crop it for you:: If the shot requires an entire browser window, app in full size, or whole screen, choose a medium size thumbnail. Use descriptions of images prior to block image macro to explain what actions the image display. Check the next page for AsciiDoc markup.

=== Use of directory and file naming conventions

To observe naming conventions consistently, retain the original title of article, use it on a subdirectory name and title of images.

Directory path for images in Fedora repos follows ~/modules/ROOT/assets/images/<ARTICLENAME_SHORTEND>/.png

.Naming subdirectory and files
[example]
If the title on H1 heading is Finding and installing Linux applications, the file name is finding-installing-linux-apps.adoc. Create a subdirectory and place images in following paths in your cloned local repo: ~/modules/ROOT/assets/images/finding-installing-linux-apps/.png

== Processing tips

Check spelling and grammar:: Check your work before creating a Pull Request.

Check the Red Hat style guide:: Use xref:contributing-docs/tools-vale-linter.adoc[Vale] to check your work to make sure it conforms to the Red Hat Style Guide.
