= AsciiDoc markup
Fedora Documentation Team <https://discussion.fedoraproject.org/tag/docs>
v1.0, 2022-12-10
:page-toclevels: 3
:page-pagination:
:description: General information about writing in AsciiDoc as well as Fedora/Antora-specific syntax that comes up often in Fedora Documentation.

This page shares general information about writing in AsciiDoc as well as Fedora/Antora-specific syntax that comes up often in Fedora Documentation.

[[basics]]
== AsciiDoc basics

[quote,asciidoctor.org]
____
AsciiDoc is a lightweight markup language for authoring notes, articles, documentation, books, web pages, slide decks and man pages in plain text.
____

https://asciidoctor.org/docs/asciidoc-syntax-quick-reference/[*AsciiDoc Syntax Quick Reference*]::
Handy cheat sheet of what the AsciiDoc markup looks like.
Use this for quick references and checking up on how to formatting, lists, media content (images and video), table of contents, and more.

https://asciidoctor.org/docs/asciidoc-recommended-practices/[*AsciiDoc Recommended Practices*]::
Best practices about writing in AsciiDoc.
Most importantly, note that https://asciidoctor.org/docs/asciidoc-recommended-practices/#one-sentence-per-line[every sentence should be on its own line].


[[snippets]]
== Fedora Documentation snippets

When you write Fedora Documentation, some things come up frequently.
They may not be documented in general AsciiDoc documentation, like on `asciidoctor.org`. +
This section contains handy references for Fedora Documentation writers to copy and paste in their own AsciiDoc documents.

[[internal-link]]
=== Internal links

In this section, we will use the following repository structure as an example:

.Example of documentation repository structure
----
📄 antora.yml <.>
📂 modules
  📂 ROOT
    📂 pages
      📄 index.adoc
      📄 another-page.adoc
      📂 sub-dir
        📄 rules.adoc
  📂 council
    📂 pages
      📄 guiding-policy.adoc
----
<.> Defines the documentation component as _test-module_ (attribute `name`)

==== Same repository

Use the local path relative to the `pages` directory in the same module.

.Link to a page at the root level
[source,adoc]
----
xref:another-page.adoc
----

.Link to a page in a subdirectory of `pages`
[source,adoc]
----
xref:sub-dir/rules.adoc
----

[[module-link]]
==== Same repository, different module

Like an internal link, but use a colon (`:`) to separate the module name.
If you are not sure if you need this, you likely don't!
Multiple modules bundled in the same repository is not currently a common scenario in Fedora Documentation.

.Example 1
[source,adoc]
----
xref:council:guiding-policy.adoc
----

[[external-antora-link]]
==== Different repository

Link to another Fedora Documentation page that exists in another repository.
Note that you *must* use the `name` field specified in the `antora.yml` file in the other repository or it will not work.
In case the target module name is `ROOT`, you can omit the name but you still need the extra colon (`:`).

.Example 1, both links are equivalent
[source,adoc]
----
xref:test-module::another-page.adoc
xref:test-module:ROOT:another-page.adoc
----

.Example 2
[source,adoc]
----
xref:test-module:council:guiding-policy.adoc
----

=== URL redirects

You can create a redirect from an old page to a new one by using the https://docs.antora.org/antora/latest/page/page-aliases/[`page-aliases`] attribute.
The syntax is the same as for xref:#internal-link[xref links].

.Example 1. In new-page.adoc 
[,adoc]
----
= Page Title
:page-aliases: old-page.adoc
----

You can also create a redirect from another module or component.

.Example 2. In new-page.adoc
[,adoc]
----
= Page Title
:page-aliases: test-module:council:removed-page.adoc
----

=== Syntax highlighting

You can add syntax highlighting to any source block by setting the source language attribute.

.Example of setting the source language attribute to a code block
------
[,yaml]
----
output:
  clean: true
  dir: ./public
  destinations:
  - provider: archive
----
------

.Example of rendered code block with such attribute
[,yaml]
----
output:
  clean: true
  dir: ./public
  destinations:
  - provider: archive
----

The list of supported languages can be found in the https://gitlab.com/fedora/docs/docs-website/ui-bundle/-/blob/main/src/js/vendor/highlight.bundle.js[highlight.bundle.js in the Fedora Docs UI].

=== Datatables

You can convert a regular table to a https://datatables.net/[DataTables] using the `datatable` role attribute.
DataTables provides filtering and ordering capabilities.

.Example of defining a DataTable
[,asciidoc]
----
[.datatable]
|===
|colA | colB | colC | colD

| yyy | 123 | zzz | 28%
| bbb | 242 | aaa | 42%
| ddd | 8874 | yyy | 99%
| ccc | 9 | ttt | 2%
| aaa | 987 | www | 18%
|===
----

.Rendered DataTable
[.datatable]
|===
|colA | colB | colC | colD

| yyy | 123 | zzz | 28%
| bbb | 242 | aaa | 42%
| ddd | 8874 | yyy | 99%
| ccc | 9 | ttt | 2%
| aaa | 987 | www | 18%
|===

Additional roles can be used to add DataTables features:

- `dt-search`: add search box
- `dt-paging`: add pagination

You can also alter the styling with the help of https://datatables.net/manual/styling/classes[built-in DataTables classes], such as `display` or `compact`.

.Example of using additional options
[,asciidoc]
----
[.datatable.dt-search.display]
|===
|colA | colB | colC | colD

| yyy | 123 | zzz | 28%
| bbb | 242 | aaa | 42%
| ddd | 8874 | yyy | 99%
|===
----

DataTables real usage can be seen on the xref:legal::not-allowed-licenses.adoc[Legal documentation].

=== Table of content
A table of content is automatically generated on the right of each pages.

IMPORTANT: There is no need to add the `:toc:` attribute as it will then add a duplicate table of content to the document.

The right-sided table of content only displays title levels up to level 2 by default.
You can change this setting with the `page-toclevels` attribute.

[,asciidoc]
----
= Page Title
:page-toclevels: 3
----

=== Pagination
If you have several pages that follow the same topic, you might be interested in enabling the pagination. +
Pagination allows the reader to easily navigate to next and previous pages from the navigation tree by adding navigation links at the bottom of the page.

This option is enabled by the `page-pagination` attribute.

[,asciidoc]
----
= Page Title
:page-pagination:
----

You can see a live example on this page.

== Best practices

A few recommendation when writing a new pages, or editing an existing one.

=== Document header
All pages **must** start with a level 1 title.
[,asciidoc]
----
= Page Title
----

Optionally, you can add https://docs.asciidoctor.org/asciidoc/latest/document/author-line/[authors] and https://docs.asciidoctor.org/asciidoc/latest/document/revision-line/[review] metadata.

.Example 1 - Authors and revision information
[,asciidoc]
----
= Page Title
Ben Cotton; Peter Boy; Petr Bokoc 
2.0, 2022-11-26: fix for F37
----

You can decide to omit the version number, if you don't need that information.

.Example 2 - Revision information without version
[,asciidoc]
----
= Page Title
Francois Andrieu
2022-12-10: Added revision metadata example
----

While these metadata are optional, try to keep at least the revision date so readers know how up-to-date the page is.

.Example 3 - Revision date only
[,asciidoc]
----
= Page Title
Fedora Documentation Team
2022-12-10
----

