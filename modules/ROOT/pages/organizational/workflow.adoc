= The Docs Workflow organization
Fedora Documentation Team <https://discussion.fedoraproject.org/tag/docs>
:revdate: 2023-04-24

[abstract]
This guide presents Docs members and aspiring contributors with the list of labels and categories associated with Issues and Merge Requests in GitLab Docs repository.

The Fedora Docs team agreed on a workflow organization, which converges at the two central pages:

https://gitlab.com/groups/fedora/docs/-/boards[GitLab dashboard] 

https://gitlab.com/groups/fedora/docs/-/merge_requests[GitLab Open Merge Requests]


== Issue board and labels

There are four labels that classify the state of an issue ticket.

* Triage: 
Issue is labeled, but no one has been assigned yet. Do you want to take over? Feel free to assign yourself and shift the issue to “In progress”.
* In progress: 
Someone has been assigned to this issue and it is in progress.
* Support needed: 
Something was already done, but someone has to support the existing assignee or take over the assignment at all.
* Approval needed: 
This is a major change and it needs an approval from someone else than the assignee. Feel free to take over the approval so that the assignee can merge afterwards.

"Open Merge Requests" do not contain a dashboard. You can see all assigned labels of an open Merge Request at first glance when opening the "Open Merge Requests" page.

Additionally, there are three labels that classify the type of an issue ticket.

* Major change: 
This is a major change within Docs page(s). It needs a Merge Request and has to be approved by someone else than the assignee before it can be merged.

* Minor change: 
This is only a minor change in Docs page(s). It does not need an approval, and it can be committed directly (a Merge Request is not mandatory).

* Internal task: 
This is an internal task of Fedora Docs that is not a change, fix or update of a Docs page/content. (Example: preparing a meeting or evaluating a survey).

Each issue ticket and each open Merge Request has to be labeled additionally with one of these three labels. The type labels do not change during the workflow.

Beyond a state label and a type label, issue tickets and merge requests can be additionally labeled as "good first issue". You want to contribute to Fedora Docs? This is your opportunity. Feel free to comment in one of these issues that you want to self-assign.

Furthermore, there is an additional label for Merge Requests that could possibly affect more than one branch, which would mean that they require follow-up Merge Requests or cherry-picks. The *multiple MR likely* label ensures that all affected branches are identified and updated, and that Merge Requests are not merged without the MR review.

== How issue tickets and Merge Requests are created

There are two ways issue tickets and Merge Requests can be created: externally and internally.

* If created externally, they are created by people from outside the Fedora Docs team, who opened them in GitLab or using the "Report an issue" (creates issue tickets) / "Edit this page" (creates Merge Requests) buttons on any Fedora Docs page (the buttons are on the top right). Tickets will appear in the list "Open" on the dashboard without any labels. The Merge Requests will appear on the Open Merge Requests page without any labels.
* If created internally, one of the Docs Team opened it. At the best, this member already assigns a *type label* and puts issue tickets into the "Triage" list, where the ticket can wait for an assignee to take over. It is the same for Merge Requests, although they do not have drag & drop lists and the "Triage" label has to be set manually just like the type label. Alternatively, the creating member can already assign an assignee to the issue ticket / Merge Request and then put (drag & drop) an issue ticket on the "In progress" list (this implies adding the "In progress" label) on the dashboard, or manually add the "In progress" label if it is a Merge Request. 

On the dashboard, issue tickets can be moved along the lists with drag and drop. The state labels change acoordingly. If a ticket is moved from "Triage" to "In progress" by drag and drop, the state label is changed from "Triage" to "In progress". Therefore, only the persistent type labels have to be set manually once when the ticket is new. This does not apply to Merge Requests on the Open Merge Request page. When creating an issue ticket on the dashboard, you will be asked where to create the issue ticket. If you are unsure where to create an issue ticket, create it in Fedora / Fedora Docs / Docs Website / Fedora Docs pages. You will see this in the "Projects" list ("Select a project") which is shown when you click to create an issue ticket. Later, this will be shown as "fedora/docs/docs-website/pages".

== The workflow
=== Minor change

If conducted by Docs members, and if it is clearly a "Minor change", a change can be done by a direct commit, without a Merge Request or issue ticket. If externals make a Merge Request as "Minor change", Docs members merge it immediately after they assigned themselves and reviewed the Merge Request. But always check if a change applies to multiple branches. 

If multiple branches are affected by a "Minor change", you are only allowed to directly merge/commit if you do the merge/commit and the cherry-picks to all affected branches at once. If you are unsure, add the "multiple MR likely" label additionally to the "Minor change" label and keep the Merge Request open for discussions.

Depending on the following discussion, the "multiple MR likely" label will be removed if there are no other affected branches and then the merge or commit can be made, or if other branches are affected, the merge or commit can be conducted and a cherry-pick to all affected branches will be done immediately and at once with the merge or commit. The goal to achieve here is that a Merge Request does not disappear from the Open Merge Request page until the update of all affected branches is complete. 

If the discussion takes place within an issue ticket (using commits instead of Merge Requests, or using multiple Merge Requests that are converged within a ticket) and not within one Merge Request, the commit and the cherry-picks can be done separately.

The workflow for a *Minor change* is as follows:

. If it is only a commit which clearly is a "Minor change", just do it. If you know what branches are affected and if you can do the commit and all related cherry-picks, go ahead. If you have not sufficient time to complete the task, proceed as follows:

. Determine the type and assign the related type label ("Minor change", "Major change", "Internal task". Here: Minor change) to the existing Merge Request or the existing issue ticket, or if non is existing yet: create one! If you are unsure what to create, create an issue ticket.

. Move new issues to the "Triage" list (which, as elaborated above, automatically assigns the "Triage" state label), or add the "Triage" label manually if it is a Merge Request.

. The issue ticket or the Merge Request can be assigned to a member who takes over. Once someone has been assigned, the ticket has to be moved to "In progress" (change to "In progress" has to be done manually for Merge Requests).

. There are a few options.
.. The assignee finishes the issue ticket/MR, and correspondingly, moves/puts it from the current state label to "Closed". If multiple branches are to be changed and if the case is handled within an MR, all branches need to be changed at once.
.. Alternatively, the assignee needs support, or additional opinions: the ticket/MR is just moved to "Support needed" to identify supporter(s) (who can, but do not have to, be assigned as additional assignee(s) if that makes sense) and once sufficient supporter(s) have been identified, move/put it back to "In progress". Use "Support needed" to encourage a discussion in the ticket/MR and to get additional opinions. Once all work is finished, the ticket/MR can be moved/put to "Closed". If multiple branches are to be changed and if the case is handled within an MR, all branches have to be changed at once. Members are free to reassign the issue (Example: if someone else has more experience with the task, or can invest more time).
.. If the assignee can no longer support the issue, the assignee either relabels the issue to "Support needed" and finds a new assignee, or makes a comment for the work completed and remaining work, removes the assignee and changes the state to "Triage".

=== Major change

"Multiple MR likely" can be transferred to "Major changes", which need an issue ticket or a Merge Request.

The workflow for a Major change is as follows:

. Determine the type and relabel Merge Request or issue ticket to *Major change*.

. Move new issues to the "Triage" list, or add the "Triage" label manually if it is a Merge Request.

. The issue ticket or the Merge Request can be reassigned to other members. Once someone has been assigned, the ticket moves to "In progress" (change to "In progress" done manually for Merge Requests).

. There are a few options.
.. The assignee finishes actively working* on the issue ticket/MR, and moves/puts it from the current state label to "Approval needed". 
.. Alternatively, the assignee needs support, or additional opinions. The ticket/MR moved to "Support needed" to identify supporter(s). Once sufficient supporter(s) volunteer, move the issue back to "In progress". "Support needed" can also be used to encourage a discussion in the ticket/MR, to get additional opinions. Once complete, the ticket/MR can be moved to "Approval needed". Change the responsible assignee to someone who has more experience with the next task, or can invest more time.

If multiple branches are affected, add the "multiple MR likely" label. If this label is assigned, the discussion within the ticket/MR has to identify all affected branches. In the end, the Merge Request has to be transferred to all affected branches through cherry-picks. If multiple branches have to be changed, all changes have to be done at once.

IMPORTANT: Do not use the "stg" branch for content. Work in dedicated forks and branches for the specific issue you are working on.

=== Internal task

Internal tasks are flexible. They start in "Open" or "Triage", and move through "Triage", "In progress", "Support needed" until "Closed".

== The role of the weekly meeting

The weekly Docs meeting helps identify and tackle issues and tasks that are not ordinary or that do not occur regularly. On the other hand, the workflow organization is set to manage the standardized day-to-day operations of Fedora Docs.

The current issue tickets and Merge Requests of the workflow organization can become regular topics on the weekly meeting. 

* Check if there are any unforeseen developments in the workflow that needs a discussion. 

* Identify and assign issue tickets and MR that remain unassigned for over two weeks, and discuss those that remain open one month after they have been assigned. 

* Use *Meeting* label for issues or MR to be discussed in the Docs meeting.
